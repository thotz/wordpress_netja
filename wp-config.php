<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpresstest');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ii@lb!iY%I?QQ?7b Qc|aM#TRs/|p;FZipFloD[[zx/Ta)[;u&;K`#L-ahQs!vjd');
define('SECURE_AUTH_KEY',  '%W#72^o2e8E(!MAGmN_3 rT#nX7iN/$.3pjCAnzxKJ_xbuelW[/,ER+*vq]*[kMS');
define('LOGGED_IN_KEY',    'Vgdp-}=wa@[#Ir$8SeNzk .T[DgJ#H^I]@?_:{?EsQ!-4Ivy|*QYJ#RWfQ9`Zk[2');
define('NONCE_KEY',        'JIhs2.YJ<$a05W2h4?D{i=ho7r ]7QIz]qE4Vy)Gb!CbGH7de9QDL(N2= h)bV8{');
define('AUTH_SALT',        'k1d|F.Rv1c7fE(9`Ab `9+>c=WbC?)Py{SGC_bpa-*<A(ComPpSA{SC)i_]jdnOX');
define('SECURE_AUTH_SALT', 'p!ix6C]WMBzg^bPG%iqS)*`il!VT=mP!wvv<mqiLW|9w%xFXSV;e:A!oAyo%Dj-N');
define('LOGGED_IN_SALT',   '$eNE+xB%.?tryr99El] .4L$nzLg*XK3tKLtab5eMf,zAQwh=3.6.V,69 f1B:Kv');
define('NONCE_SALT',       '!FZ*gg(jhg>YBE8`uqjTa%8^o!3*;]qz*NFTz60<,&5|,ZSM>APA-dq{A#Heeqlc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
