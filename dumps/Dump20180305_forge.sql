-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: forge
-- ------------------------------------------------------
-- Server version	5.7.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activations`
--

DROP TABLE IF EXISTS `activations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activations`
--

LOCK TABLES `activations` WRITE;
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
INSERT INTO `activations` VALUES (1,1,'ZxTRAIpZigSN4DyJ4C29VZPIcBlG6KmB',1,'2017-10-06 01:18:47','2017-10-06 01:18:47','2017-10-06 01:18:47'),(2,2,'hhixDgtbc4PDkIUu97pvi04HoWNlz0fi',0,NULL,'2017-10-08 18:00:00','2017-10-08 18:00:00'),(3,3,'UoJGFdn1eyZWBGedtW8y84cC89VemDQM',1,'2017-10-08 21:25:15','2017-10-08 21:25:09','2017-10-08 21:25:15');
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(6) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `activities_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=825 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES (1,'','Which activity do you mainly exercise per week (choose one from the drop-down list)?'),(2,'02001','Activity promoting arcade/video game (e.g. Wii Fit), light effort (e.g. balance yoga)'),(3,'15000','Alaska Native Games, Eskimo Olympics, general'),(4,'03050','Anishinaabe Jingle Dancing'),(5,'03060','Caribbean dance (Abakua, Beguine, Bellair, Bongo, Brukin\'s, Caribbean Quadrills, Dinki Mini, Gere, Gumbay, Ibo, Jonkonnu, Kumina, Oreisha, Jambu)'),(6,'02045','CurvesTMexercise routines in women'),(7,'02048','Elliptical trainer, moderate effort'),(8,'10010','accordion, sitting'),(9,'11003','active workstation, treadmill desk, walking'),(10,'14010','active, vigorous effort'),(11,'02003','activity promoting video game (e.g., Wii Fit), moderate effort (e.g., aerobic, resistance)'),(12,'02005','activity promoting video/arcade game (e.g., Exergaming, Dance Dance Revolution), vigorous effort'),(13,'03022','aerobic dance wearing 10-15 lb weights'),(14,'03015','aerobic, general'),(15,'03021','aerobic, high impact'),(16,'03020','aerobic, low impact'),(17,'03017','aerobic, step, with 10 - 12 inch step'),(18,'03018','aerobic, step, with 4-inch step'),(19,'03016','aerobic, step, with 6 - 8 inch step'),(20,'11006','airline flight attendant'),(21,'06010','airplane repair'),(22,'05197','animal care, household animals, general'),(23,'15010','archery, non-hunting'),(24,'02008','army type obstacle course exercise, boot camp training program'),(25,'15192','auto racing, open wheel'),(26,'06020','automobile body work'),(27,'16010','automobile or light truck (not a semi) driving'),(28,'06030','automobile repair, light or moderate effort'),(29,'17010','backpacking (Taylor Code 050)'),(30,'17012','backpacking, hiking or organized walking with a daypack'),(31,'15020','badminton, competitive (Taylor Code 450)'),(32,'15030','badminton, social singles and doubles, general'),(33,'11010','bakery, general, moderate effort'),(34,'11015','bakery, light effort'),(35,'03010','ballet, modern, or jazz, general, rehearsal or class'),(36,'03012','ballet, modern, or jazz, performance, vigorous effort'),(37,'03038','ballroom dancing, competitive, general'),(38,'03030','ballroom, fast (Taylor Code 125)'),(39,'03040','ballroom, slow (e.g., waltz, foxtrot, slow dancing, samba, tango, 19thcentury dance, mambo, cha cha)'),(40,'15072','basketball, drills, practice'),(41,'15040','basketball, game (Taylor Code 490)'),(42,'15055','basketball, general'),(43,'15050','basketball, non-game, general (Taylor Code 480)'),(44,'15060','basketball, officiating (Taylor Code 500)'),(45,'15070','basketball, shooting baskets'),(46,'15075','basketball, wheelchair'),(47,'13010','bathing, sitting'),(48,'03019','bench step class, general'),(49,'01020','bicycling, 10-11.9 mph, leisure, slow, light effort'),(50,'01065','bicycling, 12 mph, seated, hands on brake hoods or bar drops, 80 rpm'),(51,'01066','bicycling, 12 mph, standing, hands on brake hoods, 60 rpm'),(52,'01030','bicycling, 12-13.9 mph, leisure, moderate effort'),(53,'01040','bicycling, 14-15.9 mph, racing or leisure, fast, vigorous effort'),(54,'01050','bicycling, 16-19 mph, racing/not drafting or &gt; 19 mph drafting, very fast, racing general'),(55,'01010','bicycling, &lt;10 mph, leisure, to work or for pleasure (Taylor Code 115)'),(56,'01060','bicycling, &gt; 20 mph, racing, not drafting'),(57,'01008','bicycling, BMX'),(58,'01015','bicycling, general'),(59,'01018','bicycling, leisure, 5.5 mph'),(60,'01019','bicycling, leisure, 9.4 mph'),(61,'01004','bicycling, mountain, competitive, racing'),(62,'01009','bicycling, mountain, general'),(63,'01003','bicycling, mountain, uphill, vigorous'),(64,'01013','bicycling, on dirt or farm road, moderate pace'),(65,'02013','bicycling, stationary, 101-160 watts, vigorous effort'),(66,'02014','bicycling, stationary, 161-200 watts, vigorous effort'),(67,'02015','bicycling, stationary, 201-270 watts, very vigorous effort'),(68,'02011','bicycling, stationary, 30-50 watts, very light to light effort'),(69,'02017','bicycling, stationary, 51-89 watts, light-to-moderate effort'),(70,'02012','bicycling, stationary, 90-100 watts, moderate to vigorous effort'),(71,'02019','bicycling, stationary, RPM/Spin bike class'),(72,'02010','bicycling, stationary, general'),(73,'01011','bicycling, to/from work, self selected pace'),(74,'15080','billiards'),(75,'17085','bird watching, slow walk'),(76,'09000','board game playing, sitting'),(77,'18010','boating, power, driving'),(78,'18012','boating, power, passenger, light'),(79,'11020','bookbinding'),(80,'15090','bowling (Taylor Code 390)'),(81,'15092','bowling, indoor, bowling alley'),(82,'15100','boxing, in ring, general'),(83,'15110','boxing, punching bag'),(84,'15120','boxing, sparring'),(85,'05189','breastfeeding, sitting or reclining'),(86,'15130','broomball'),(87,'05149','building a fire inside'),(88,'11035','building road, directing traffic, standing'),(89,'11030','building road, driving heavy machinery'),(90,'05045','butchering animal, large, vigorous effort'),(91,'05044','butchering animals, small'),(92,'02020','calisthenics (e.g., push ups, sit ups, pull-ups, jumping jacks), vigorous effort'),(93,'02022','calisthenics (e.g., push ups, sit ups, pull-ups, lunges), moderate effort'),(94,'02024','calisthenics (e.g., situps, abdominal crunches), light effort'),(95,'02030','calisthenics, light or moderate effort, general (example: back exercises), going up &amp; down from floor (Taylor Code 150)'),(96,'09110','camping involving standing, walking, sitting, light-to-moderate effort'),(97,'18025','canoeing, harvesting wild rice, knocking rice off the stalks'),(98,'18020','canoeing, on camping trip (Taylor Code 270)'),(99,'18030','canoeing, portaging'),(100,'18040','canoeing, rowing, 2.0-3.9 mph, light effort'),(101,'18050','canoeing, rowing, 4.0-5.9 mph, moderate effort'),(102,'18070','canoeing, rowing, for pleasure, general (Taylor Code 250)'),(103,'18080','canoeing, rowing, in competition, or crew or sculling (Taylor Code 260)'),(104,'18060','canoeing, rowing, kayaking, competition, &gt;6 mph, vigorous effort'),(105,'09010','card playing,sitting'),(106,'06060','carpentry, finishing or refinishing cabinets or furniture'),(107,'11042','carpentry, general, heavy or vigorous effort'),(108,'11038','carpentry, general, light effort'),(109,'11040','carpentry, general, moderate effort'),(110,'06040','carpentry, general, workshop (Taylor Code 620)'),(111,'06074','carpentry, home remodeling tasks, light effort'),(112,'06072','carpentry, home remodeling tasks, moderate effort'),(113,'06052','carpentry, outside house, building a fence'),(114,'06050','carpentry, outside house, installing rain gutters (Taylor Code 640),carpentry, outside house, building a fence'),(115,'06070','carpentry, sawing hardwood'),(116,'17026','carrying 1 to 15 lb load, upstairs'),(117,'17021','carrying 15 lb child, slow walking'),(118,'17020','carrying 15 pound load (e.g. suitcase), level ground or downstairs'),(119,'17027','carrying 16 to 24 lb load, upstairs'),(120,'17028','carrying 25 to 49 lb load, upstairs'),(121,'17029','carrying 50 to 74 lb load, upstairs'),(122,'17030','carrying &gt; 74 lb load, upstairs'),(123,'05056','carrying groceries upstairs'),(124,'11050','carrying heavy loads (e.g., bricks, tools)'),(125,'17025','carrying load upstairs, general'),(126,'11060','carrying moderate loads up stairs, moving boxes 25-49 lbs'),(127,'08010','carrying, loading or stacking wood, loading/unloading or carrying lumber'),(128,'08009','carrying, loading or stacking wood, loading/unloading or carrying lumber, light-to-moderate effort'),(129,'09005','casino gambling, standing'),(130,'06080','caulking, chinking log cabin'),(131,'06090','caulking, except log cabin'),(132,'10020','cello, sitting'),(133,'11070','chambermaid, hotel housekeeper, making bed, cleaning bathroom, pushing cart'),(134,'15138','cheerleading, gymnastic moves, competitive '),(135,'09013','chess game, sitting'),(136,'05184','child care, infant, general'),(137,'05185','child care, sitting/kneeling (e.g., dressing, bathing, grooming, feeding, occasional lifting of child), light effort, general'),(138,'05186','child care, standing (e.g., dressing, bathing, grooming, feeding, occasional lifting of child), moderate effort'),(139,'15135','childrens games, adults playing (e.g., hopscotch, 4-square, dodge ball, playground apparatus, t-ball, tetherball, marbles, jacks, arcade games), moderate effort'),(140,'08019','chopping wood, splitting logs, moderate effort'),(141,'08020','chopping wood, splitting logs, vigorous effort'),(142,'02040','circuit training, including kettlebells, some aerobic movement with minimal rest, general, vigorous intensity'),(143,'02035','circuit training, moderate effort'),(144,'20060','cleaning church'),(145,'06100','cleaning gutters'),(146,'05022','cleaning windows, washing windows, general'),(147,'05040','cleaning, general (straightening up, changing linen, carrying out trash, light effort'),(148,'05020','cleaning, heavy or major (e.g. wash car, wash windows, clean garage), moderate effort'),(149,'05030','cleaning, house or cabin, general, moderate effort'),(150,'05021','cleaning, mopping, standing, moderate effort'),(151,'05010','cleaning, sweeping carpet or floors, general'),(152,'05011','cleaning, sweeping, slow, light effort'),(153,'05012','cleaning, sweeping, slow, moderateeffort'),(154,'08030','clearing brush/land, undergrowth, or ground, hauling branches, wheelbarrow chores, vigorous effort'),(155,'08025','clearing light brush, thinning garden, moderate effort'),(156,'17035','climbing hills with 0 to 9 lb load'),(157,'17040','climbing hills with 10 to 20 lb load'),(158,'17050','climbing hills with 21 to 42 lb load'),(159,'17060','climbing hills with 42+ lb load'),(160,'17033','climbing hills, no load'),(161,'15142','coaching, actively playing sport with players'),(162,'15140','coaching, football, soccer, basketball, baseball, swimming, etc.'),(163,'11080','coal mining, drilling coal, rock'),(164,'11090','coal mining, erecting supports'),(165,'11100','coal mining, general'),(166,'11110','coal mining, shoveling coal'),(167,'10030','conducting orchestra, standing'),(168,'11120','construction, outside, remodeling, new structures (e.g., roof repair, miscellaneous'),(169,'11115','cook, chef'),(170,'05057','cooking Indian bread on an outside stove'),(171,'05050','cooking or food preparation - standing or sitting or in general (not broken into stand/walk components), manual appliances, light effort'),(172,'05049','cooking or food preparation, moderate effort'),(173,'05052','cooking or food preparation, walking'),(174,'09015','copying documents, standing'),(175,'15150','cricket, batting, bowling, fielding'),(176,'15160','croquet'),(177,'15170','curling'),(178,'11121','custodial work, light effort (e.g., cleaning sink and toilet, dusting, vacuuming, light cleaning)'),(179,'11125','custodial work, light effort (e.g., cleaning sink and toilet, dusting, vacuuming, light cleaning)'),(180,'11126','custodial work, moderate effort (e.g., electric buffer, feathering arena floors, mopping, taking out trash, vacuuming)'),(181,'05046','cutting and smoking fish, drying fish or meat'),(182,'15180','darts, wall or lawn'),(183,'17070','descending stairs'),(184,'08040','digging sandbox, shoveling sand'),(185,'08050','digging, spading, filling garden, compositing, (Taylor Code 590)'),(186,'08045','digging, spading, filling garden, composting, light-to-moderate effort'),(187,'08052','digging, spading, filling garden, composting, vigorous effort'),(188,'18090','diving, springboard or platform'),(189,'19005','dog sledding, mushing'),(190,'19006','dog sledding, passenger'),(191,'10035','double bass, standing'),(192,'15190','drag racing, pushing or driving a car'),(193,'09020','drawing, writing, painting, standing'),(194,'13020','dressing, undressing, standing or sitting'),(195,'11128','driving delivery truck, taxi, shuttle bus, school bus'),(196,'08055','driving tractor'),(197,'10045','drumming (e.g., bongo, conga, benbe), moderate, sitting'),(198,'10040','drums, sitting'),(199,'05032','dusting or polishing furniture, general'),(200,'20050','eating at church'),(201,'13030','eating, sitting'),(202,'20055','eating/talking at church or standing eating, American Indian Feast days'),(203,'05200','elder care, disabled adult, bathing, dressing, moving into and out of bed, only active periods'),(204,'05205','elder care, disabled adult, feeding, combing hair, light effort, only active periods'),(205,'11130','electrical work (e.g., hook up wire, tapping-splicing)'),(206,'11135','engineer (e.g., mechanical or electrical)'),(207,'03025','ethnic or cultural dancing (e.g., Greek, Middle Eastern, hula, salsa, merengue, bamba y plena, flamenco, belly, and swing)'),(208,'06110','excavating garage'),(209,'11160','farming, driving harvester, cutting hay, irrigation work'),(210,'11170','farming, driving tasks (e.g., driving tractor or harvester)'),(211,'11190','farming, feeding cattle, horses'),(212,'11180','farming, feeding small animals'),(213,'11191','farming, hauling water for animals, general hauling water'),(214,'11147','farming, light effort (e.g., cleaning animal sheds, preparing animal feed)'),(215,'11210','farming, milking by hand, cleaning pails, moderate effort'),(216,'11220','farming, milking by machine, light effort'),(217,'11146','farming, moderate effort (e.g., feeding animals, chasing cattle by walking and/or horseback, spreading manure, harvesting crops)'),(218,'11195','farming, rice, planting, grain milling activities'),(219,'11192','farming, taking care of animals (e.g., grooming, brushing, shearing sheep, assisting with birthing, medical care, branding), general'),(220,'11145','farming, vigorous effort (e.g., baling hay, cleaning barn)'),(221,'05053','feeding household animals'),(222,'08057','felling trees, large size'),(223,'08058','felling trees, small-medium size'),(224,'15200','fencing'),(225,'11240','fire fighter, general'),(226,'11246','fire fighter, hauling hoses on ground, carrying/hoisting equipment, breaking down walls, wearing full gear'),(227,'11245','fire fighter, raising and climbing ladder with full gear, simulated fire suppression'),(228,'11244','fire fighter, rescue victim, automobile accident, using pike pole'),(229,'04030','fishing from boat or canoe, sitting'),(230,'04020','fishing from river bank and walking'),(231,'04040','fishing from river bank, standing (Taylor Code 660)'),(232,'04050','fishing in stream, in waders (Taylor Code 670)'),(233,'04010','fishing related, digging worms, with shovel'),(234,'04065','fishing with a spear, standing'),(235,'04007','fishing, catching fish with hands'),(236,'11247','fishing, commercial, light effort'),(237,'11248','fishing, commercial, moderate effort'),(238,'11249','fishing, commercial, vigorous effort'),(239,'04005','fishing, crab fishing'),(240,'04062','fishing, dip net, setting net and retrieving fish, general'),(241,'04064','fishing, fishing wheel, setting net and retrieving fish, general'),(242,'04001','fishing, general'),(243,'04060','fishing, ice, sitting'),(244,'04061','fishing, jog or line, standing, general'),(245,'10050','flute, sitting'),(246,'16020','flying airplane or helicopter'),(247,'05060','food shopping with or without a grocery cart, standing or walking'),(248,'15235','football or baseball, playing catch'),(249,'15210','football, competitive'),(250,'15230','football, touch, flag, general (Taylor Code 510)'),(251,'15232','football, touch, flag, light effort'),(252,'11262','forestry, ax chopping, fast, 1.25 kg axe, 35 blows/min, vigorous effort'),(253,'11260','forestry, ax chopping, slow,1.25 kg axe, 19 blows/min, moderate effort'),(254,'11250','forestry, ax chopping, very fast, 1.25 kg axe, 51 blows/min, extremely vigorous effort'),(255,'11264','forestry, moderate effort (e.g., sawing wood with power saw, weeding, hoeing)'),(256,'11266','forestry, vigorous effort (e.g., barking, felling, or trimming trees, carrying or stacking logs, felling trees, planting seeds, sawing lumber by hand )'),(257,'15240','frisbee playing, general'),(258,'15250','frisbee, ultimate'),(259,'11370','furriery'),(260,'11375','garbage collector, walking, dumping bins into truck'),(261,'08060','gardening with heavy power tools, tilling a garden, chain saw'),(262,'08245','gardening, general, moderate effort'),(263,'08065','gardening, using containers, older adults &gt; 60 years'),(264,'03031','general dancing (e.g., disco, folk, Irish step dancing, line dancing, polka, contra, country)'),(265,'20061','general yard work at church'),(266,'14020','general, moderate effort'),(267,'13000','getting ready for bed, general, standing'),(268,'15255','golf, general'),(269,'15270','golf, miniature, driving range'),(270,'15290','golf, using power cart (Taylor Code 070)'),(271,'15265','golf, walking, carrying clubs'),(272,'15285','golf, walking, pulling clubs'),(273,'13040','grooming, washing hands, shaving,brushing teeth, putting on make-up, sitting or standing'),(274,'10120','guitar, classical, folk, sitting'),(275,'10125','guitar, rock and roll band, standing'),(276,'15300','gymnastics, general'),(277,'15310','hacky sack'),(278,'13045','hairstyling, standing'),(279,'11378','hairstylist (e.g., plaiting hair, manicure, make-up artist)'),(280,'06124','hammering nails'),(281,'15320','handball, general (Taylor Code 520)'),(282,'15330','handball, team'),(283,'15340','hang gliding'),(284,'06122','hanging sheet rock inside house'),(285,'06120','hanging storm windows'),(286,'13046','having hair or nails done by someone else, sitting'),(287,'02061','health club exercise classes, general, gym/weight training combined in one visit'),(288,'02062','health club exercise, conditioning classes'),(289,'02060','health club exercise, general (Taylor Code 160)'),(290,'15335','high ropes course, multiple elements'),(291,'17082','hiking or walking at a normal pace through fields and hillsides'),(292,'17080','hiking, cross country (Taylor Code 040)'),(293,'15350','hockey, field'),(294,'15362','hockey, ice, competitive'),(295,'15360','hockey, ice, general'),(296,'02064','home exercise, general'),(297,'06126','home repair, general, light effort'),(298,'06127','home repair, general, moderate effort'),(299,'06128','home repair, general, vigorous effort'),(300,'10060','horn, standing'),(301,'15408','horse cart, driving, standing or sitting'),(302,'15375','horse chores, feeding, watering, cleaning stalls, implied walking and lifting loads'),(303,'11380','horse grooming, includingfeeding, cleaning stalls, bathing, brushing, clipping,longeing and exercising horses.'),(304,'11390','horse racing, galloping'),(305,'11400','horse racing, galloping'),(306,'11410','horse racing, walking'),(307,'11381','horse, feeding, watering, cleaning stalls, implied walking and lifting loads'),(308,'15395','horseback riding, canter or gallop'),(309,'15370','horseback riding, general'),(310,'15402','horseback riding, jumping'),(311,'15390','horseback riding, trotting'),(312,'15400','horseback riding,walking'),(313,'15410','horseshoe pitching, quoits'),(314,'04086','hunting large game from a car, plane, or boat'),(315,'04081','hunting large game, dragging carcass'),(316,'04085','hunting large game, from a hunting stand, limited walking'),(317,'04083','hunting large marine animals'),(318,'04115','hunting, birds'),(319,'04070','hunting, bow and arrow, or crossbow'),(320,'04080','hunting, deer, elk, large game (Taylor Code 170)'),(321,'04090','hunting, duck, wading'),(322,'04095','hunting, flying fox, squirrel'),(323,'04100','hunting, general'),(324,'04125','hunting, hiking with hunting gear'),(325,'04110','hunting, pheasants or grouse (Taylor Code 680)'),(326,'04123','hunting, pigs, wild'),(327,'04120','hunting, rabbit, squirrel, prairie chick, raccoon, small game (Taylor Code 690)'),(328,'19011','ice fishing, sitting'),(329,'05147','implied walking, putting away household items, moderate effort'),(330,'08250','implied walking/standing - picking up yard, light, picking flowers or vegetables'),(331,'05070','ironing'),(332,'08070','irrigation channels, opening and closing ports'),(333,'04063','ishing, set net, setting net and retrieving fish, general'),(334,'15420','jai alai'),(335,'18160','jet skiing, driving, in water'),(336,'12010','jog/walk combination (jogging component of less than 10 minutes) (Taylor Code 180)'),(337,'12020','jogging, general'),(338,'12025','jogging, in place'),(339,'12027','jogging, on a mini-tramp'),(340,'15440','juggling'),(341,'18100','kayaking, moderate effort'),(342,'15450','kickball'),(343,'05035','kitchen activity, general, (e.g., cooking, washing dishes, cleaning up), moderate effort'),(344,'11413','kitchen maid'),(345,'20025','kneeling in church or at home, praying'),(346,'05080','knitting, sewing, light effort, wrapping presents, sitting'),(347,'15460','lacrosse'),(348,'09025','laughing, sitting'),(349,'11418','laundry worker'),(350,'05090','laundry, fold or hang clothes, put clothes in washer or dryer, packing suitcase, washing clothes by hand, implied standing, light effort'),(351,'05092','laundry, hanging wash, washing clothes by hand, moderate effort'),(352,'05095','laundry, putting away clothes, gathering clothes to pack, putting away laundry, implied walking'),(353,'15465','lawn bowling, bocce ball, outdoor'),(354,'11415','lawn keeper, yard work, general'),(355,'08080','laying crushed rock'),(356,'06130','laying or removing carpet'),(357,'08090','laying sod'),(358,'06140','laying tile or linoleum,repairing appliances'),(359,'17031','loading /unloading a car, implied walking'),(360,'11420','locksmith'),(361,'07010','lying quietly and watching television'),(362,'07011','lying quietly, doing nothing, lying in bed awake, listening to music (not talking or reading)'),(363,'11430','machine tooling (e.g., machining, working sheet metal, machine fitter, operating lathe, welding) light-to-moderate effort'),(364,'11450','machine tooling, operating punch press, moderate effort'),(365,'05100','making bed, changing linens'),(366,'11472','manager, property'),(367,'11475','manual or unskilled labor, general, light effort'),(368,'11476','manual or unskilled labor, general, moderate effort'),(369,'11477','manual or unskilled labor, general, vigorous effort'),(370,'05110','maple syruping/sugar bushing (including carrying buckets, carrying wood)'),(371,'10130','marching band, baton twirling, walking, moderate pace, general'),(372,'10135','marching band, drum major, walking'),(373,'10131','marching band, playing an instrument, walking, brisk pace, general'),(374,'17090','marching rapidly, military, no pack'),(375,'17088','marching, moderate speed, military, no pack'),(376,'15430','martial arts, different types, moderate pace (e.g., judo, jujitsu, karate, kick boxing, tae kwan do, tai-bo, Muay Thai boxing)'),(377,'15425','martial arts, different types, slower pace, novice performers, practice'),(378,'11482','masonry, concrete, light effort'),(379,'11480','masonry, concrete, moderate effort'),(380,'11485','massage therapist, standing'),(381,'07075','meditating'),(382,'05023','mopping, standing, light effort'),(383,'15470','moto-cross, off-road motor sports, all-terrain vehicle, general'),(384,'16030','motor scooter, motorcycle'),(385,'05120','moving furniture, household items, carrying boxes'),(386,'05150','moving household items upstairs, carrying boxes or furniture'),(387,'19010','moving ice house, set up/drill holes'),(388,'11490','moving, carrying or pushing heavy objects, 75 lbs or more, only active time (e.g., desks, moving van work)'),(389,'05121','moving, lifting light loads'),(390,'08095','mowing lawn, general'),(391,'08125','mowing lawn, power mower, light or moderate effort (Taylor Code 590)'),(392,'08100','mowing lawn, riding mower (Taylor Code 550)'),(393,'08110','mowing lawn, walk, hand mower (Taylor Code 570)'),(394,'08120','mowing lawn, walk, power mower, moderate or vigorous effort'),(395,'05025','multiple household tasks all at once, light effort'),(396,'05026','multiple household tasks all at once, moderate effort'),(397,'05027','multiple household tasks all at once, vigorous effort'),(398,'02200','native New Zealander physical activities (e.g., Haka Powhiri, Moteatea, Waita Tira, Whakawatea, etc.) , general, moderate effort'),(399,'02205','native New Zealander physical activities (e.g., Haka, Taiahab), general, vigorous effort'),(400,'05065','non-food shopping, with or without a cart, standing or walking'),(401,'11500','operating heavy duty equipment, automated, not driving'),(402,'08130','operating snow blower, walking'),(403,'11510','orange grove work, picking fruit'),(404,'10077','organ, sitting'),(405,'05125','organizing room'),(406,'15480','orienteering'),(407,'18225','paddle boarding, standing'),(408,'18110','paddle boat'),(409,'15500','paddleball, casual, general (Taylor Code 460)'),(410,'15490','paddleball, competitive'),(411,'06160','painting inside house,wallpapering, scraping paint'),(412,'06165','painting, (Taylor Code 630)'),(413,'06150','painting, outside home (Taylor Code 650)'),(414,'11514','painting,house, furniture, moderate effort'),(415,'14030','passive, light effort, kissing, hugging'),(416,'10070','piano, sitting'),(417,'08248','picking fruit off trees, gleaning fruits, picking fruits/vegetables, climbing ladder to pick fruit, vigorous effort'),(418,'08246','picking fruit off trees, picking fruits/vegetables, moderate effort'),(419,'02105','pilates, general'),(420,'04130','pistol shooting or trap shooting, standing'),(421,'08145','planting crops or garden, stooping, moderate effort'),(422,'08140','planting seedlings, shrub, stooping, moderate effort'),(423,'08150','planting trees'),(424,'08135','planting, potting, transplanting seedlings or plants, light effort'),(425,'10074','playing musical instruments, general'),(426,'11516','plumbing activities'),(427,'06167','plumbing, general'),(428,'11525','police, directing traffic, standing'),(429,'11526','police, driving a squad car, sitting'),(430,'11528','police, making an arrest, standing'),(431,'11527','police, riding in a squad car, sitting'),(432,'05024','polishing floors, standing, walking slowly, using electric polishing machine'),(433,'15510','polo, on horseback'),(434,'11529','postal carrier, walking to deliver mail'),(435,'20040','praise with dance or run, spiritual dancing in church'),(436,'20046','preparing food at church'),(437,'11520','printing, paper industry worker, standing'),(438,'16035','pulling rickshaw'),(439,'17105','pushing a wheelchair, non-occupational'),(440,'17100','pushing or pulling stroller with child or walking with children, 2.5 to 3.1 mph'),(441,'16040','pushing plane in and out of hangar'),(442,'06170','put on and removal of tarp - sailboat'),(443,'05055','putting away groceries (e.g. carrying groceries, shopping without a grocery cart), carrying packages'),(444,'17110','race walking'),(445,'15520','racquetball, competitive'),(446,'15530','racquetball, general (Taylor Code 470)'),(447,'08165','raking lawn (Taylor Code 600)'),(448,'08160','raking lawn or leaves, moderate effort'),(449,'08170','raking roof with snow rake'),(450,'05188','reclining with baby'),(451,'07070','reclining, reading'),(452,'07060','reclining, talking or talking on phone'),(453,'07050','reclining, writing'),(454,'06144','repairing appliances'),(455,'02054','resistance (weight) training, multiple exercises, 8-15 repetitions at varied resistance '),(456,'02052','resistance (weight) training, squats , slow or explosive effort'),(457,'02050','resistance training (weight lifting - free weight, nautilus or universal-type), power lifting or body building, vigorous effort (Taylor Code 210)'),(458,'09101','retreat/family reunion activities involving playing games with children'),(459,'09100','retreat/family reunion activities involving sitting, relaxing, talking, eating'),(460,'16016','riding in a bus or train'),(461,'16015','riding in a car or truck'),(462,'08180','riding snow blower'),(463,'04145','rifle exercises, shooting, kneeling or standing'),(464,'04140','rifle exercises, shooting, lying down'),(465,'15537','rock climbing, ascending or traversing rock, low-to-moderate difficulty'),(466,'15535','rock climbing, ascending rock, high difficulty'),(467,'15540','rock climbing, rappelling'),(468,'15533','rock or mountain climbing (Taylor Code 470)'),(469,'15542','rodeo sports, general, light effort'),(470,'15544','rodeo sports, general, moderate effort'),(471,'15546','rodeo sports, general, vigorous effort'),(472,'15591','rollerblading, in-line skating, 14.4 km/h (9.0 mph), recreational pace'),(473,'15592','rollerblading, in-line skating, 17.7 km/h (11.0 mph), moderate pace, exercise training'),(474,'15593','rollerblading, in-line skating, 21.0 to 21.7 km/h (13.0 to 13.6 mph), fast pace, exercise training'),(475,'15594','rollerblading, in-line skating, 24.0 km/h (15.0 mph), maximal effort'),(476,'06180','roofing'),(477,'15550','rope jumping, fast pace, 120-160 skips/min'),(478,'15551','rope jumping, moderate pace, 100-120 skips/min, general, 2 foot skip, plain bounce'),(479,'15552','rope jumping, slow pace, &lt; 100 skips/min, 2 foot skip, rhythm bounce'),(480,'02068','rope skipping, general'),(481,'02070','rowing, stationary ergometer, general, vigorous effort'),(482,'02072','rowing, stationary, 100 watts, moderate effort'),(483,'02073','rowing, stationary, 150 watts, vigorous effort'),(484,'02074','rowing, stationary, 200 watts, very vigorous effort'),(485,'02071','rowing, stationary, general, moderate effort'),(486,'15562','rugby, touch, non-competitive'),(487,'15560','rugby, union, team, competitive'),(488,'12150','running, (Taylor code 200)'),(489,'12120','running, 10 mph (6 min/mile)'),(490,'12130','running, 11 mph (5.5 min/mile)'),(491,'12132','running, 12 mph (5 min/mile)'),(492,'12134','running, 13 mph (4.6 min/mile)'),(493,'12135','running, 14 mph (4.3 min/mile)'),(494,'12029','running, 4 mph (15 min/mile)'),(495,'12030','running, 5 mph (12 min/mile)'),(496,'12040','running, 5.2 mph (11.5 min/mile)'),(497,'12050','running, 6 mph (10 min/mile)'),(498,'12060','running, 6.7 mph (9 min/mile)'),(499,'12070','running, 7 mph (8.5 min/mile)'),(500,'12080','running, 7.5 mph (8 min/mile)'),(501,'12090','running, 8 mph (7.5 min/mile)'),(502,'12100','running, 8.6 mph (7 min/mile)'),(503,'12110','running, 9 mph (6.5 min/mile)'),(504,'12140','running, cross country'),(505,'12200','running, marathon'),(506,'12180','running, on a track, team practice'),(507,'12170','running, stairs, up'),(508,'12190','running, training, pushing a wheelchair or baby carrier'),(509,'08190','sacking grass, leaves'),(510,'15380','saddling, cleaning, grooming, harnessing and unharnessing horse'),(511,'18140','sailing, Sunfish/Laser/Hobby Cat, Keel boats, ocean sailing, yachting, leisure'),(512,'18120','sailing, boat and board sailing, windsurfing, ice sailing, general (Taylor Code 235)'),(513,'18130','sailing, in competition'),(514,'06190','sanding floors with a power sander'),(515,'06200','scraping and painting sailboat or powerboat'),(516,'05131','scrubbing floors, on hands and knees, scrubbing bathroom, bathtub, light effort'),(517,'05130','scrubbing floors, on hands and knees, scrubbing bathroom, bathtub, moderate effort'),(518,'05132','scrubbing floors, on hands and knees, scrubbing bathroom, bathtub, vigorous effort'),(519,'20045','serving food at church'),(520,'05051','serving food, setting table, implied walking or standing'),(521,'05082','sewing with a machine'),(522,'06205','sharpening tools'),(523,'11530','shoe repair, general'),(524,'08192','shoveling dirt or mud'),(525,'08195','shoveling snow, by hand, moderate effort'),(526,'08202','shoveling snow, by hand, vigorous effort'),(527,'11570','shoveling, 10 to 15 pounds/minute, vigorous effort'),(528,'11540','shoveling, digging ditches'),(529,'11560','shoveling, less than 10 pounds/minute, moderate effort'),(530,'11550','shoveling, more than 16 pounds/minute, deep digging, vigorous effort'),(531,'08200','shovelling snow, by hand (Taylor Code 610)'),(532,'13050','showering, toweling off, standing'),(533,'15570','shuffleboard'),(534,'05190','sit, playing with animals, light effort, only active periods'),(535,'07026','sitting at a desk, resting head in hands'),(536,'09115','sitting at a sporting event, spectator'),(537,'20000','sitting in church, in service, attending a ceremony, sitting quietly'),(538,'20005','sitting in church, talking or singing, attending a ceremony, sitting, active participation'),(539,'11585','sitting meetings, light effort, general, and/or with talking involved (e.g., eating at a business meeting)'),(540,'13009','sitting on toilet, eliminating while standing or squating'),(541,'07020','sitting quietly and watching television'),(542,'07022','sitting quietly, fidgeting, general, fidgeting hands'),(543,'07021','sitting quietly, general'),(544,'11580','sitting tasks, light effort (e.g., office work, chemistry lab work, computer work, light assembly repair, watch repair, reading, desk work)'),(545,'11590','sitting tasks, moderate effort (e.g., pushing heavy levers, riding mower/forklift, crane operation)'),(546,'09075','sitting, arts and crafts, carving wood, weaving, spinning wool, light effort'),(547,'09080','sitting, arts and crafts, carving wood, weaving, spinning wool, moderate effort'),(548,'21016','sitting, child care, only active periods'),(549,'07023','sitting, fidgeting feet'),(550,'09065','sitting, in class, general, including note-taking or class discussion'),(551,'21005','sitting, light office work, in general'),(552,'07025','sitting, listening to music (not talking or reading) or watching a movie in a theater'),(553,'21000','sitting, meeting, general, and/or with talking involved'),(554,'21010','sitting, moderate work'),(555,'20001','sitting, playing an instrument at church'),(556,'09045','sitting, playing traditional video game, computer game'),(557,'05170','sitting, playing with child(ren), light effort, only active periods'),(558,'20010','sitting, reading religious materials at home'),(559,'09030','sitting, reading, book, newspaper, etc.'),(560,'07024','sitting, smoking'),(561,'09060','sitting, studying, general, including reading and/or writing, light effort'),(562,'09055','sitting, talking in person, on the phone, computer, or text messaging, light effort'),(563,'11593','sitting, teaching stretching or yoga, or light effort exercise class'),(564,'09040','sitting, writing, desk work, typing'),(565,'15582','skateboarding, competitive, vigorous effort'),(566,'15580','skateboarding, general, moderate effort'),(567,'19018','skating, ice dancing'),(568,'19020','skating, ice, 9 mph or less'),(569,'19030','skating, ice, general (Taylor Code 360)'),(570,'19040','skating, ice, rapidly, more than 9 mph, not competitive'),(571,'15590','skating, roller (Taylor Code 360)'),(572,'19050','skating, speed, competitive'),(573,'19060','ski jumping, climb up carrying skis'),(574,'02080','ski machine, general'),(575,'19080','skiing, cross country, 2.5 mph, slow or light effort, ski walking'),(576,'19090','skiing, cross country, 4.0-4.9 mph, moderate speed and effort, general'),(577,'19100','skiing, cross country, 5.0-7.9 mph, brisk speed, vigorous effort'),(578,'19110','skiing, cross country, &gt;8.0 mph, elite skier, racing'),(579,'19130','skiing, cross country, hard snow, uphill, maximum, snow mountaineering'),(580,'19140','skiing, cross-country, biathlon, skating technique'),(581,'19135','skiing, cross-country, skating'),(582,'19150','skiing, downhill, alpine or snowboarding, light effort, active time only'),(583,'19160','skiing, downhill, alpine or snowboarding, moderate effort, general, active time only'),(584,'19170','skiing, downhill, vigorous effort, racing'),(585,'19075','skiing, general'),(586,'19175','skiing, roller, elite racers'),(587,'18150','skiing, water or wakeboarding (Taylor Code 220)'),(588,'11495','skindiving or SCUBA diving as a frogman, Navy Seal'),(589,'18180','skindiving, fast'),(590,'18190','skindiving, moderate'),(591,'18200','skindiving, scuba diving, general (Taylor Code 310)'),(592,'15600','skydiving, base jumping, bungee jumping'),(593,'19180','sledding, tobogganing, bobsledding, luge (Taylor Code 370)'),(594,'07030','sleeping'),(595,'02085','slide board exercise, general'),(596,'02090','slimnastics, jazzercise'),(597,'18210','snorkeling (Taylor Code 310)'),(598,'19260','snow blower, walking and pushing'),(599,'19190','snow shoeing, moderate effort'),(600,'19192','snow shoeing, vigorous effort'),(601,'19252','snow shoveling, by hand, moderate effort'),(602,'19254','snow shoveling, by hand, vigorous effort'),(603,'19200','snowmobiling, driving, moderate'),(604,'19202','snowmobiling, passenger'),(605,'15610','soccer, casual, general (Taylor Code 540)'),(606,'15605','soccer, competitive'),(607,'15620','softball or baseball, fast or slow pitch, general (Taylor Code 440)'),(608,'15630','softball, officiating'),(609,'15625','softball, practice'),(610,'15640','softball,pitching'),(611,'15645','sports spectator, very excited, emotional, physically moving'),(612,'06210','spreading dirt with a shove'),(613,'15650','squash (Taylor Code 530)'),(614,'15652','squash, general '),(615,'17134','stair climbing, fast pace'),(616,'17133','stair climbing, slow pace'),(617,'17130','stair climbing, using or climbing up ladder (Taylor Code 030)'),(618,'02065','stair-treadmill ergometer, general'),(619,'05191','stand, playing with animals, light effort, only active periods'),(620,'20015','standing quietly in church, attending a ceremony'),(621,'07040','standing quietly, standing in a line'),(622,'11600','standing tasks, light effort (e.g., bartending, store clerk, assembling, filing, duplicating, librarian, putting up a Christmas tree, standing and talking at work, changing clothes when teaching physical education,standing)'),(623,'09085','standing, arts and crafts, sand painting, carving, weaving, light effort'),(624,'09090','standing, arts and crafts, sand painting, carving, weaving, moderate effort'),(625,'09095','standing, arts and crafts, sand painting, carving, weaving, vigorous effort'),(626,'05195','standing, bathing dog'),(627,'21017','standing, child care, only active periods'),(628,'07041','standing, fidgeting'),(629,'05183','standing, holding child'),(630,'05160','standing, light effort tasks (pump gas, change light bulb, etc.)'),(631,'21015','standing, light work (filing, talking, assembling)'),(632,'11610','standing, light/moderate effort (e.g., assemble/repair heavy parts, welding,stocking parts,auto repair,standing, packing boxes, nursing patient care)'),(633,'21020','standing, light/moderate work (e.g., pack boxes, assemble/repair, set up chairs/furniture)'),(634,'09071','standing, miscellaneous'),(635,'21025','standing, moderate (lifting 50 lbs., assembling at fast rate)'),(636,'20065','standing, moderate effort (e.g., lifting heavy objects, assembling at fast rate)'),(637,'11620','standing, moderate effort, intermittent lifting 50 lbs, hitch/twisting ropes'),(638,'11615','standing, moderate effort, lifting items continuously, 10-20 lbs, with limited walking or resting'),(639,'20095','standing, moderate-to-heavy effort, manual labor, lifting 50 lbs, heavy maintenance'),(640,'11630','standing, moderate/heavy tasks (e.g., lifting more than 50 lbs, masonry, painting, paper hanging)'),(641,'21030','standing, moderate/heavy work'),(642,'05146','standing, packing/unpacking boxes, occasional lifting of lightweight household items, loading or unloading items in car, moderate effort'),(643,'05171','standing, playing with child(ren) light effort, only active periods'),(644,'09070','standing, reading'),(645,'20020','standing, singing in church, attending a ceremony, standing, active participation'),(646,'20030','standing, talking in church'),(647,'09050','standing, talking in person, on the phone, computer, or text messaging, light effort'),(648,'11708','steel mill, moderate effort (e.g., fettling, forging, tipping molds)'),(649,'11710','steel mill, vigorous effort (e.g., hand rolling, merchant mill rolling, removing slag, tending furnace)'),(650,'02101','stretching, mild'),(651,'18222','surfing, body or board, competitive'),(652,'18220','surfing, body or board, general'),(653,'05140','sweeping garage, sidewalk or outside of house'),(654,'18230','swimming laps, freestyle, fast, vigorous effort'),(655,'18240','swimming laps, freestyle, front crawl, slow, light or moderate effort'),(656,'18250','swimming, backstroke, general, training or competition'),(657,'18255','swimming, backstroke, recreational'),(658,'18260','swimming, breaststroke, general, training or competition'),(659,'18265','swimming, breaststroke, recreational'),(660,'18270','swimming, butterfly, general'),(661,'18280','swimming, crawl, fast speed, ~75 yards/minute, vigorous effort'),(662,'18290','swimming, crawl, medium speed, ~50 yards/minute, vigorous effort'),(663,'18300','swimming, lake, ocean, river (Taylor Codes 280, 295)'),(664,'18310','swimming, leisurely, not lap swimming, general'),(665,'18320','swimming, sidestroke, general'),(666,'18330','swimming, synchronized'),(667,'18340','swimming, treading water, fast, vigorous effort'),(668,'18350','swimming, treading water, moderate effort, general'),(669,'15660','table tennis, ping pong (Taylor Code 410)'),(670,'15670','tai chi, qi gong, general'),(671,'15672','tai chi, qi gong, sitting, light effort'),(672,'11720','tailoring, cutting fabric'),(673,'11730','tailoring, general'),(674,'11740','tailoring, hand sewing'),(675,'11750','tailoring, machine sewing'),(676,'11760','tailoring, pressing'),(677,'11763','tailoring, weaving, light effort (e.g., finishing operations, washing, dyeing, inspecting cloth, counting yards, paperwork)'),(678,'11765','tailoring, weaving, moderate effort (e.g., spinning and weaving operations, delivering boxes of yam to spinners, loading of warp bean, pinwinding, conewinding, warping, cloth cutting)'),(679,'13036','taking medication, sitting or standing'),(680,'13035','talking and eating or eating only, standing'),(681,'05048','tanning hides, general'),(682,'03014','tap'),(683,'02110','teaching exercise class (e.g., aerobic, water)'),(684,'15685','tennis, doubles'),(685,'15680','tennis, doubles (Taylor Code 430)'),(686,'15675','tennis, general'),(687,'15695','tennis, hitting balls, non-game play, moderate effort'),(688,'15690','tennis, singles (Taylor Code 420)'),(689,'02112','therapeutic exercise ball, Fitball exercise'),(690,'09105','touring/traveling/vacation involving riding in a vehicle'),(691,'09106','touring/traveling/vacation involving walking'),(692,'15733','track and field (e.g., high jump, long jump, triple jump, javelin, pole vault)'),(693,'15732','track and field (e.g., shot, discus, hammer throw)'),(694,'15734','track and field (e.g., steeplechase, hurdles)'),(695,'15702','trampoline, competitive'),(696,'15700','trampoline, recreational'),(697,'04124','trapping game, general'),(698,'08210','trimming shrubs or trees, manual cutter'),(699,'08215','trimming shrubs or trees, power cutter, using leaf blower, edge, moderate effort'),(700,'10080','trombone, standing'),(701,'11766','truck driving, loading and unloading truck, tying down load, standing, walking and carrying heavy loads'),(702,'16050','truck, semi, tractor, &gt; 1 ton, or bus, driving'),(703,'10090','trumpet, standing'),(704,'18352','tubing, floating on a river, general'),(705,'11770','typing, electric, manual or computer'),(706,'20100','typing, electric, manual, or computer'),(707,'21035','typing, electric, manual, or computer'),(708,'01070','unicycling'),(709,'02115','upper body exercise, arm ergometer'),(710,'02117','upper body exercise, stationary bicycle - Airdyne (arms only) 40 rpm, moderate'),(711,'17140','using crutches'),(712,'11780','using heavy power tools such as pneumatic tools (e.g., jackhammers, drills)'),(713,'11790','using heavy tools (not power) such as shovel, pick, tunnel bar, spade'),(714,'05043','vacuuming, general, moderate effort'),(715,'02143','video exercise workouts, TV conditioning programs (e.g., cardio-resistance), moderate effort'),(716,'02146','video exercise workouts, TV conditioning programs (e.g., cardio-resistance), vigorous effort'),(717,'02140','video exercise workouts, TV conditioning programs (e.g., yoga, stretching), light effort'),(718,'10100','violin, sitting'),(719,'15710','volleyball (Taylor Code 400)'),(720,'15725','volleyball, beach, in sand'),(721,'15711','volleyball, competitive, in gymnasium'),(722,'15720','volleyball, non-competitive, 6 - 9 member team, general'),(723,'21018','walk/run play with children, moderate, only active periods'),(724,'21019','walk/run play with children, vigorous, only active periods'),(725,'05192','walk/run, playing with animals, general, light effort, only active periods'),(726,'05193','walk/run, playing with animals, moderate effort, only active periods'),(727,'05194','walk/run, playing with animals, vigorous effort, only active periods'),(728,'20039','walk/stand combination for religious purposes, usher'),(729,'21070','walk/stand combination, for volunteer purposes'),(730,'05181','walking and carrying small child, child weighing 15 lbs or more'),(731,'05182','walking andcarrying small child, child weighing less than 15 lbs'),(732,'17160','walking for pleasure (Taylor Code 010)'),(733,'16060','walking for transportation, 2.8-3.2 mph, level, moderate pace, firm surface'),(734,'17161','walking from house to car or bus, from car or bus to go places, from car or bus to and from the worksite'),(735,'20035','walking in church'),(736,'11795','walking on job, 2.5 mph, slow speed and carrying light objects less than 25 pounds'),(737,'11792','walking on job, 3.0 mph, in office, moderate speed, not carrying anything'),(738,'11793','walking on job, 3.5 mph, in office, brisk speed, not carrying anything'),(739,'11791','walking on job, less than 2.0 mph, very slow speed, in office or lab area'),(740,'11850','walking or walk downstairs or standing, carrying objects about 100 pounds or over'),(741,'11820','walking or walk downstairs or standing, carrying objects about 25 to 49 pounds'),(742,'11830','walking or walk downstairs or standing, carrying objects about 50 to 74 pounds'),(743,'11840','walking or walk downstairs or standing, carrying objects about 75 to 99 pounds'),(744,'17165','walking the dog'),(745,'17162','walking to neighbors house or familys house for social reasons'),(746,'17152','walking, 2.0 mph, level, slow pace, firm surface'),(747,'21055','walking, 2.5 mph slowly and carrying objects less than 25 pounds'),(748,'17180','walking, 2.5 mph, downhill'),(749,'17170','walking, 2.5 mph, level, firm surface'),(750,'11797','walking, 2.5 mph, slow speed, carrying heavy objects more than 25 lbs'),(751,'17190','walking, 2.8 to 3.2 mph, level, moderate pace, firm surface'),(752,'17210','walking, 2.9 to 3.5 mph, uphill, 1 to 5% grade'),(753,'17211','walking, 2.9 to 3.5 mph, uphill, 6% to 15% grade'),(754,'21060','walking, 3.0 mph moderately and carrying objects less than 25 pounds, pushing something'),(755,'20037','walking, 3.0 mph, moderate speed, not carrying anything'),(756,'21045','walking, 3.0 mph, moderate speed, not carrying anything'),(757,'11800','walking, 3.0 mph, moderately and carrying light objects less than 25 lbs'),(758,'20038','walking, 3.5 mph, brisk speed, not carrying anything'),(759,'21050','walking, 3.5 mph, brisk speed, not carrying anything'),(760,'11810','walking, 3.5 mph, briskly and carrying objects less than 25 pounds'),(761,'21065','walking, 3.5 mph, briskly and carrying objects less than 25 pounds'),(762,'17200','walking, 3.5 mph, level, brisk, firm surface, walking for exercise'),(763,'17220','walking, 4.0 mph, level, firm surface, very brisk pace'),(764,'17230','walking, 4.5 mph, level, firm surface, very, very brisk'),(765,'17231','walking, 5.0 mph, level, firm surface'),(766,'17235','walking, 5.0 mph, uphill, 3% grade'),(767,'08220','walking, applying fertilizer or seeding a lawn, push applicator'),(768,'17320','walking, backwards, 3.5 mph, level'),(769,'17325','walking, backwards, 3.5 mph, uphill, 5% grade'),(770,'17302','walking, for exercise, 3.5 to 4 mph, with ski poles, Nordic walking, level, moderate pace'),(771,'17305','walking, for exercise, 5.0 mph, with ski poles, Nordic walking, level, fast pace'),(772,'17310','walking, for exercise, with ski poles, Nordic walking, uphill'),(773,'17250','walking, for pleasure, work break'),(774,'08251','walking, gathering gardening tools'),(775,'11796','walking, gathering things at work, ready to leave'),(776,'17260','walking, grass track'),(777,'17150','walking, household'),(778,'17151','walking, less than 2.0 mph, level, strolling, very slow'),(779,'20036','walking, less than 2.0 mph, very slow'),(780,'21040','walking, less than 2.0 mph, very slow'),(781,'17262','walking, normal pace, plowed field or sand'),(782,'11805','walking, pushing a wheelchair'),(783,'17280','walking, to and from an outhouse'),(784,'17270','walking, to work or class (Taylor Code 015'),(785,'05165','walking,moderate effort tasks, non-cleaning (readying to leave, shut/lock doors, close windows, etc.)'),(786,'05175','walking/running, playing with child(ren), moderate effort, only active periods'),(787,'05180','walking/running, playing with child(ren), vigorous effort, only active periods'),(788,'15731','wallyball, general'),(789,'05042','wash dishes, clearing dishes from table, walking, light effort'),(790,'05041','wash dishes, standing or in general (not broken into stand/walk components)'),(791,'06225','washing and waxing car'),(792,'06220','washing and waxing hull of sailboat or airplane'),(793,'20047','washing dishes, cleaning kitchen at church'),(794,'06230','washing fence, painting fence, moderate effort'),(795,'18355','water aerobics, water calisthenics'),(796,'02120','water aerobics, water calisthenics, water exercise'),(797,'18366','water jogging'),(798,'18360','water polo'),(799,'18365','water volleyball'),(800,'18367','water walking, light effort, slow pace'),(801,'18368','water walking, moderate effort, moderate pace'),(802,'18369','water walking, vigorous effort, brisk pace'),(803,'08230','watering lawn or garden, standing or walking'),(804,'05148','watering plants'),(805,'08240','weeding, cultivating garden (Taylor Code 580)'),(806,'08239','weeding, cultivating garden, light-to-moderate effort'),(807,'08241','weeding, cultivating garden, using a hoe, moderate-to-vigorous effort'),(808,'08255','wheelbarrow, pushing garden cart or wheelbarrow'),(809,'02135','whirlpool, sitting'),(810,'18370','whitewater rafting, kayaking, or canoeing'),(811,'18385','windsurfing or kitesurfing, crossing trial'),(812,'18390','windsurfing, competition, pumping for speed'),(813,'18380','windsurfing, not pumping for speed'),(814,'06240','wiring, tapping-splicing'),(815,'10110','woodwind, sitting'),(816,'11870','working in scene shop, theater actor, backstage employee'),(817,'15722','wrestling (one match is 5 minutes)'),(818,'08260','yard work, general, light effort'),(819,'08261','yard work, general, moderate effort'),(820,'08262','yard work, general, vigorous effort'),(821,'02150','yoga, Hatha'),(822,'02170','yoga, Nadisodhana'),(823,'02160','yoga, Power'),(824,'02180','yoga, Surya Namaskar');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `type` smallint(6) DEFAULT NULL,
  `profile_id` int(10) unsigned DEFAULT NULL,
  `region_id` int(10) unsigned DEFAULT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_country_code_index` (`country_code`),
  KEY `addresses_profile_id_foreign` (`profile_id`),
  CONSTRAINT `addresses_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (1,NULL,NULL,NULL,1,NULL,'SG',NULL,NULL,NULL),(2,'11',NULL,NULL,2,NULL,'PH','222222','2017-10-08 17:59:59','2017-10-08 17:59:59'),(3,NULL,NULL,NULL,3,3089,'PH',NULL,'2017-10-08 21:25:09','2017-10-08 21:25:09');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_outbox`
--

DROP TABLE IF EXISTS `email_outbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_outbox` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=>Outbox, 1=>Sent, 2=>Failed, 3=>Bounce',
  `attempts` tinyint(4) NOT NULL DEFAULT '0',
  `from_user_id` int(10) unsigned DEFAULT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email_outbox_from_user_id_foreign` (`from_user_id`),
  KEY `email_outbox_to_user_id_foreign` (`to_user_id`),
  KEY `email_outbox_created_by_foreign` (`created_by`),
  KEY `email_outbox_updated_by_foreign` (`updated_by`),
  KEY `email_outbox_deleted_by_foreign` (`deleted_by`),
  CONSTRAINT `email_outbox_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `email_outbox_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `email_outbox_from_user_id_foreign` FOREIGN KEY (`from_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `email_outbox_to_user_id_foreign` FOREIGN KEY (`to_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `email_outbox_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_outbox`
--

LOCK TABLES `email_outbox` WRITE;
/*!40000 ALTER TABLE `email_outbox` DISABLE KEYS */;
INSERT INTO `email_outbox` VALUES (1,'56adde4bfdb7ad6c8bf77bea18c3faa4_1507514400',1,1,1,2,'hello@example.com','admin_1@sprim.com',NULL,NULL,'Welcome to HealthFactr','<html>\n<head>\n	<!-- If you delete this meta tag, Half Life 3 will never be released. -->\n	<meta name=\"viewport\" content=\"width=device-width\" />\n	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n	<title>HealthFactr Email</title>\n</head>\n<body bgcolor=\"#f7f7f7\" style=\"font-family:Arial; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; margin:0\">\n<!-- HEADER -->\n<table style=\"width: 100%;\">\n<tr>\n	<td></td>\n	<td>\n		<div style=\"padding:0px; max-width:640px; margin:0 auto; display:block;\">\n			<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n			<tr>\n				<td align=\"right\" style=\"display: block;\" ><img src=\"http://localhost/assets//images/logo.png\" style=\"height:auto !important; max-width:640px !important; width: 100% !important; display:block; margin:0;\" /></td>\n			</tr>\n			</table>\n		</div>\n		<div style=\"padding:0px; max-width:640px; margin:0 auto; display:block;\">\n			<table style=\"border-collapse: collapse;table-layout: fixed;margin-left: auto;margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #7cc9db;background-position: 0px 0px;background-image: url(http://localhost/assets/images/email_banner.jpg);background-repeat: repeat;\" align=\"center\">\n			<tbody>\n			<tr>\n				<td class=\"column\" style=\"padding: 0;text-align: left;vertical-align: top;color: #8e8e8e;font-size: 14px;line-height: 21px;font-family: Cabin,Avenir,sans-serif;\" width=\"640\">\n				<div style=\"margin-left: 20px; margin-right: 20px;margin-top: 24px;\">\n					<div style=\"line-height:35px;font-size:1px\">&nbsp;</div>\n				</div>\n				<div style=\"margin-left: 20px;margin-right: 20px;\">\n					<div style=\"line-height:13px;font-size:1px\">&nbsp;</div>\n				</div>\n				<div style=\"margin-left: 20px;margin-right: 20px;\">\n					<div style=\"line-height:13px;font-size:1px\">&nbsp;</div>\n				</div>\n				<div style=\"margin-left: 20px;margin-right: 20px;\">\n					<div style=\"line-height:13px;font-size:1px\">&nbsp;</div>\n				</div>\n				<div style=\"margin-left: 20px;margin-right: 20px;\">\n					<h1 class=\"size-34\" style=\"margin-top: 0;margin-bottom: 20px;font-style: normal;font-weight: normal;color: #4badd1;font-size: 25px;line-height: 43px;font-family: &quot;Open Sans&quot;,sans-serif;text-align: center;\"><span style=\"color:#ffffff\"><strong>\n						Account Activation\n</strong></span></h1>\n				</div>\n				<div style=\"margin-left: 20px;margin-right: 20px;margin-bottom: 24px;\">\n					<div style=\"line-height:58px;font-size:1px\">&nbsp;</div>\n				</div>\n				</td>\n			</tr>\n			</tbody>\n			</table>\n		</div>\n	</td>\n	<td></td>\n</tr>\n</table><!-- /HEADER -->\n\n<!-- BODY -->\n<table style=\"width: 100%;\">\n<tr>\n	<td></td>\n	<td class=\"container\" bgcolor=\"#f7f7f7\">\n		<div style=\"padding:15px; max-width:600px; margin:0 auto; display:block;\">\n		<table>\n		<tr>\n			<td>\n<p style=\"font-size:14px; color:#000; line-height: 1.4; margin-bottom:27px;\">\n	Dear Admin_1 Admin_1,\n</p>\n\n<p style=\"font-size:16px; line-height: 1.4; color:#0979b5; margin-bottom:27px;\">\n	Your account in HealthFactr has been created.\n</p>\n\n<table style=\"margin-bottom:27px; font-size:14px; color:#707070;\" width=\"100%\">\n	<tr>\n        <td style=\"border-bottom:#9e9e9e solid 1px; padding:8px;\">Name: Admin_1 Admin_1</td>\n    </tr>\n    <tr>\n        <td style=\"border-bottom:#9e9e9e solid 1px; padding:8px;\">Email: admin_1@sprim.com</td>\n    </tr>\n    <tr>\n        <td style=\"border-bottom:#9e9e9e solid 1px; padding:8px;\">Activation Link: <a href=\"http://healthfactr.local/user/activate/2/hhixDgtbc4PDkIUu97pvi04HoWNlz0fi\">Activate</a></td>\n    </tr>\n</table>\n\n				<p style=\"font-size:16px; color:#707070; margin-bottom:0;\">Regards,</p>\n				<p style=\"font-size:25px; margin-top:0px; margin-bottom:-4px; color:#0979b5 \">HealthFactr Team</p>\n			</td>\n		</tr>\n		</table>\n		</div><!-- /content -->\n	</td>\n	<td></td>\n</tr>\n</table><!-- /BODY -->\n\n<!-- FOOTER -->\n<table style=\"width: 100%;clear:both!important; height:59px;\">\n<tr>\n	<td></td>\n	<td style=\"padding-top:0px;\">\n		<!-- content -->\n		<div style=\"padding:15px; max-width:640px; margin:0 auto; display:block; border-top:20px solid #0979b5;\">\n		<table width=\"100%\">\n		<tr>\n			<td align=\"center\">\n				<p style=\"padding-top:15px; color:#0979b5;\">\n					<a href=\"http://localhost/emails/agents/56adde4bfdb7ad6c8bf77bea18c3faa4_1507514400\" style=\"color:#0979b5; font-size:14px; text-decoration:none;\">View In Browser</a>\n				</p>\n			</td>\n		</tr>\n		</table>\n		</div><!-- /content -->\n	</td>\n	<td></td>\n</tr>\n</table><!-- /FOOTER -->\n\n</body>\n</html>',1,NULL,NULL,'2017-10-08 18:00:00','2017-10-08 18:00:00',NULL),(2,'d9440762bc057094b8c4477e45aec926_1507526709',1,1,1,3,'hello@example.com','hcp_1@gmail.com',NULL,NULL,'Welcome to HealthFactr','<html>\n<head>\n	<!-- If you delete this meta tag, Half Life 3 will never be released. -->\n	<meta name=\"viewport\" content=\"width=device-width\" />\n	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n	<title>HealthFactr Email</title>\n</head>\n<body bgcolor=\"#f7f7f7\" style=\"font-family:Arial; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; margin:0\">\n<!-- HEADER -->\n<table style=\"width: 100%;\">\n<tr>\n	<td></td>\n	<td>\n		<div style=\"padding:0px; max-width:640px; margin:0 auto; display:block;\">\n			<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n			<tr>\n				<td align=\"right\" style=\"display: block;\" ><img src=\"http://localhost/assets//images/logo.png\" style=\"height:auto !important; max-width:640px !important; width: 100% !important; display:block; margin:0;\" /></td>\n			</tr>\n			</table>\n		</div>\n		<div style=\"padding:0px; max-width:640px; margin:0 auto; display:block;\">\n			<table style=\"border-collapse: collapse;table-layout: fixed;margin-left: auto;margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #7cc9db;background-position: 0px 0px;background-image: url(http://localhost/assets/images/email_banner.jpg);background-repeat: repeat;\" align=\"center\">\n			<tbody>\n			<tr>\n				<td class=\"column\" style=\"padding: 0;text-align: left;vertical-align: top;color: #8e8e8e;font-size: 14px;line-height: 21px;font-family: Cabin,Avenir,sans-serif;\" width=\"640\">\n				<div style=\"margin-left: 20px; margin-right: 20px;margin-top: 24px;\">\n					<div style=\"line-height:35px;font-size:1px\">&nbsp;</div>\n				</div>\n				<div style=\"margin-left: 20px;margin-right: 20px;\">\n					<div style=\"line-height:13px;font-size:1px\">&nbsp;</div>\n				</div>\n				<div style=\"margin-left: 20px;margin-right: 20px;\">\n					<div style=\"line-height:13px;font-size:1px\">&nbsp;</div>\n				</div>\n				<div style=\"margin-left: 20px;margin-right: 20px;\">\n					<div style=\"line-height:13px;font-size:1px\">&nbsp;</div>\n				</div>\n				<div style=\"margin-left: 20px;margin-right: 20px;\">\n					<h1 class=\"size-34\" style=\"margin-top: 0;margin-bottom: 20px;font-style: normal;font-weight: normal;color: #4badd1;font-size: 25px;line-height: 43px;font-family: &quot;Open Sans&quot;,sans-serif;text-align: center;\"><span style=\"color:#ffffff\"><strong>\n						Account Activation\n</strong></span></h1>\n				</div>\n				<div style=\"margin-left: 20px;margin-right: 20px;margin-bottom: 24px;\">\n					<div style=\"line-height:58px;font-size:1px\">&nbsp;</div>\n				</div>\n				</td>\n			</tr>\n			</tbody>\n			</table>\n		</div>\n	</td>\n	<td></td>\n</tr>\n</table><!-- /HEADER -->\n\n<!-- BODY -->\n<table style=\"width: 100%;\">\n<tr>\n	<td></td>\n	<td class=\"container\" bgcolor=\"#f7f7f7\">\n		<div style=\"padding:15px; max-width:600px; margin:0 auto; display:block;\">\n		<table>\n		<tr>\n			<td>\n<p style=\"font-size:14px; color:#000; line-height: 1.4; margin-bottom:27px;\">\n	Dear Hcp_1 Hcp_1,\n</p>\n\n<p style=\"font-size:16px; line-height: 1.4; color:#0979b5; margin-bottom:27px;\">\n	Your account in HealthFactr has been created.\n</p>\n\n<table style=\"margin-bottom:27px; font-size:14px; color:#707070;\" width=\"100%\">\n	<tr>\n        <td style=\"border-bottom:#9e9e9e solid 1px; padding:8px;\">Name: Hcp_1 Hcp_1</td>\n    </tr>\n    <tr>\n        <td style=\"border-bottom:#9e9e9e solid 1px; padding:8px;\">Email: hcp_1@gmail.com</td>\n    </tr>\n    <tr>\n        <td style=\"border-bottom:#9e9e9e solid 1px; padding:8px;\">Activation Link: <a href=\"http://healthfactr.local/user/activate/3/UoJGFdn1eyZWBGedtW8y84cC89VemDQM\">Activate</a></td>\n    </tr>\n</table>\n\n				<p style=\"font-size:16px; color:#707070; margin-bottom:0;\">Regards,</p>\n				<p style=\"font-size:25px; margin-top:0px; margin-bottom:-4px; color:#0979b5 \">HealthFactr Team</p>\n			</td>\n		</tr>\n		</table>\n		</div><!-- /content -->\n	</td>\n	<td></td>\n</tr>\n</table><!-- /BODY -->\n\n<!-- FOOTER -->\n<table style=\"width: 100%;clear:both!important; height:59px;\">\n<tr>\n	<td></td>\n	<td style=\"padding-top:0px;\">\n		<!-- content -->\n		<div style=\"padding:15px; max-width:640px; margin:0 auto; display:block; border-top:20px solid #0979b5;\">\n		<table width=\"100%\">\n		<tr>\n			<td align=\"center\">\n				<p style=\"padding-top:15px; color:#0979b5;\">\n					<a href=\"http://localhost/emails/agents/d9440762bc057094b8c4477e45aec926_1507526709\" style=\"color:#0979b5; font-size:14px; text-decoration:none;\">View In Browser</a>\n				</p>\n			</td>\n		</tr>\n		</table>\n		</div><!-- /content -->\n	</td>\n	<td></td>\n</tr>\n</table><!-- /FOOTER -->\n\n</body>\n</html>',1,NULL,NULL,'2017-10-08 21:25:09','2017-10-08 21:25:09',NULL);
/*!40000 ALTER TABLE `email_outbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ethnic_origins`
--

DROP TABLE IF EXISTS `ethnic_origins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ethnic_origins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ethnic_origins_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ethnic_origins`
--

LOCK TABLES `ethnic_origins` WRITE;
/*!40000 ALTER TABLE `ethnic_origins` DISABLE KEYS */;
INSERT INTO `ethnic_origins` VALUES (1,'001','European',NULL,NULL),(2,'002','Eastern Indian',NULL,NULL),(3,'003','Asian',NULL,NULL),(4,'004','American Indian',NULL,NULL),(5,'005','African',NULL,NULL),(6,'006','Melanesian',NULL,NULL),(7,'007','Micronesian',NULL,NULL),(8,'008','Polynesian',NULL,NULL),(9,'009','Australian Aborigine',NULL,NULL);
/*!40000 ALTER TABLE `ethnic_origins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_owners`
--

DROP TABLE IF EXISTS `file_owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_owners` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `owner_id` int(10) unsigned DEFAULT NULL,
  `owner_table` tinyint(4) NOT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  KEY `file_owners_file_id_foreign` (`file_id`),
  KEY `file_owners_deleted_by_foreign` (`deleted_by`),
  KEY `file_owners_created_by_foreign` (`created_by`),
  CONSTRAINT `file_owners_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `file_owners_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `file_owners_file_id_foreign` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_owners`
--

LOCK TABLES `file_owners` WRITE;
/*!40000 ALTER TABLE `file_owners` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_owners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `file_type` tinyint(4) NOT NULL,
  `description` varchar(255) NOT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `files_name_unique` (`name`),
  KEY `files_deleted_by_foreign` (`deleted_by`),
  CONSTRAINT `files_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hcp_specialties`
--

DROP TABLE IF EXISTS `hcp_specialties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hcp_specialties` (
  `hcp_id` int(10) unsigned NOT NULL,
  `specialty_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`hcp_id`,`specialty_id`),
  CONSTRAINT `hcp_specialties_hcp_id_foreign` FOREIGN KEY (`hcp_id`) REFERENCES `hcps` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hcp_specialties`
--

LOCK TABLES `hcp_specialties` WRITE;
/*!40000 ALTER TABLE `hcp_specialties` DISABLE KEYS */;
/*!40000 ALTER TABLE `hcp_specialties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hcps`
--

DROP TABLE IF EXISTS `hcps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hcps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(10) unsigned DEFAULT NULL,
  `mcr` varchar(255) DEFAULT NULL,
  `graduation_year` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hcps_profile_id_foreign` (`profile_id`),
  CONSTRAINT `hcps_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hcps`
--

LOCK TABLES `hcps` WRITE;
/*!40000 ALTER TABLE `hcps` DISABLE KEYS */;
INSERT INTO `hcps` VALUES (1,3,'MCR1',1985,'2017-10-08 21:25:09','2017-10-08 21:25:09');
/*!40000 ALTER TABLE `hcps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `health_tests`
--

DROP TABLE IF EXISTS `health_tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `health_tests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` int(10) unsigned DEFAULT NULL,
  `hcp_id` int(10) unsigned DEFAULT NULL,
  `input_params` longtext,
  `api_response` longtext,
  `doc_recom` longtext,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `health_tests_created_by_foreign` (`created_by`),
  KEY `health_tests_patient_id_foreign` (`patient_id`),
  KEY `health_tests_hcp_id_foreign` (`hcp_id`),
  CONSTRAINT `health_tests_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `health_tests_hcp_id_foreign` FOREIGN KEY (`hcp_id`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `health_tests_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `health_tests`
--

LOCK TABLES `health_tests` WRITE;
/*!40000 ALTER TABLE `health_tests` DISABLE KEYS */;
/*!40000 ALTER TABLE `health_tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2015_07_02_230147_migration_cartalyst_sentinel',1),(4,'2016_10_13_054156_application',1),(5,'2016_11_03_114255_activities',1),(6,'2016_11_08_070121_drrechealthtests',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(10) unsigned DEFAULT NULL,
  `country` varchar(3) DEFAULT NULL,
  `ethnic_origin` varchar(5) DEFAULT NULL,
  `drug_allergy` text NOT NULL,
  `remarks` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patients_profile_id_foreign` (`profile_id`),
  CONSTRAINT `patients_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persistences`
--

DROP TABLE IF EXISTS `persistences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistences`
--

LOCK TABLES `persistences` WRITE;
/*!40000 ALTER TABLE `persistences` DISABLE KEYS */;
INSERT INTO `persistences` VALUES (1,1,'iEWXFgFZhRB92SpbyvYlYzgddz1r7VNt','2017-10-06 01:19:25','2017-10-06 01:19:25'),(2,1,'crWIHYRCENrgLZXNy2jfuzP0UwRqHDbf','2017-10-06 01:19:25','2017-10-06 01:19:25'),(3,1,'lvgE2IQxsYjaNmcb0lkoUCGckebUU897','2017-10-08 17:58:19','2017-10-08 17:58:19'),(4,1,'Fzgz4Q2hQhbth3bTKbP1LH6MX6iHNF3X','2017-10-08 17:58:19','2017-10-08 17:58:19');
/*!40000 ALTER TABLE `persistences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_contacts`
--

DROP TABLE IF EXISTS `profile_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_contacts` (
  `profile_id` int(10) unsigned DEFAULT NULL,
  `info` varchar(60) NOT NULL,
  `type` smallint(6) NOT NULL,
  KEY `profile_contacts_profile_id_foreign` (`profile_id`),
  CONSTRAINT `profile_contacts_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_contacts`
--

LOCK TABLES `profile_contacts` WRITE;
/*!40000 ALTER TABLE `profile_contacts` DISABLE KEYS */;
INSERT INTO `profile_contacts` VALUES (1,'111111111',1),(2,'admin_1@sprim.com',3),(2,'1234567890',1),(3,'hcp_1@gmail.com',3);
/*!40000 ALTER TABLE `profile_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL COMMENT 'NOT_KNOWN(0), MALE(1), FEMALE(2), NOT_APPLICABLE(9)',
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_user_id_foreign` (`user_id`),
  CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,1,1,'Super',NULL,'1977-01-01','2017-10-06 01:18:47',NULL),(2,2,1,'Admin_1','Admin_1','1970-01-01','2017-10-08 17:59:59','2017-10-08 17:59:59'),(3,3,1,'Hcp_1','Hcp_1','1967-01-01','2017-10-08 21:25:09','2017-10-08 21:25:09');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders`
--

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_users`
--

LOCK TABLES `role_users` WRITE;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
INSERT INTO `role_users` VALUES (1,1,'2017-10-06 01:18:47','2017-10-06 01:18:47'),(2,2,'2017-10-08 18:00:00','2017-10-08 18:00:00'),(3,3,'2017-10-08 21:25:09','2017-10-08 21:25:09');
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `permissions` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'master','master','{\"user.create\":true,\"user.edit\":true,\"user.delete\":true,\"user.view\":true,\"role.create\":true,\"role.edit\":true,\"role.delete\":true,\"role.view\":true}','2017-10-06 01:18:46','2017-10-06 01:18:46'),(2,'admin','admin','{\"user.create\":true,\"user.edit\":true,\"user.delete\":true,\"user.view\":true}','2017-10-06 01:18:47','2017-10-06 01:18:47'),(3,'hcp','hcp','{\"user.create\":false,\"user.edit\":false,\"user.delete\":false,\"user.view\":false}','2017-10-06 01:18:47','2017-10-06 01:18:47'),(4,'patient','patient','{\"user.create\":false,\"user.edit\":false,\"user.delete\":false,\"user.view\":false}','2017-10-06 01:18:47','2017-10-06 01:18:47');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `throttle`
--

DROP TABLE IF EXISTS `throttle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `throttle`
--

LOCK TABLES `throttle` WRITE;
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;
/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `reset_password` tinyint(1) NOT NULL DEFAULT '0',
  `reset_password_code` varchar(10) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Super','Sprim','sprim.super@gmail.com','$2y$10$NgDeIJ10y/eg0/OqM3D/9OLOW9V1.7CcLbWynsf/3UiQDU/u4rYBS','2017-10-08 17:58:19',1,0,NULL,NULL,1,NULL,'2017-10-06 01:18:47','2017-10-08 17:58:19'),(2,'Admin_1','Admin_1','admin_1@sprim.com','$2y$10$NgDeIJ10y/eg0/OqM3D/9OLOW9V1.7CcLbWynsf/3UiQDU/u4rYBS',NULL,1,1,'72087424',NULL,1,NULL,'2017-10-08 17:59:59','2017-10-08 17:59:59'),(3,'Hcp_1','Hcp_1','hcp_1@gmail.com','$2y$10$tNyngeF6PLcufh2/9DfonebPRua5xSFA0ClrMfibcXWakNO.1OSFq',NULL,1,1,'24697197',NULL,1,1,'2017-10-08 21:25:08','2017-10-08 21:25:15');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-05 17:37:19
